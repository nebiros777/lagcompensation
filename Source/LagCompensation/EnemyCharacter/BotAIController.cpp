// Fill out your copyright notice in the Description page of Project Settings.


#include "BotAIController.h"


void ABotAIController::BeginPlay()
{
	Super::BeginPlay();

	if (BehaviorTree.LoadSynchronous())
	{
		RunBehaviorTree(BehaviorTree.Get());
	}
}