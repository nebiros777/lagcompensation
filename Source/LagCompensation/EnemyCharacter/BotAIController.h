// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BotAIController.generated.h"

/**
 * 
 */
UCLASS()
class LAGCOMPENSATION_API ABotAIController : public AAIController
{
	GENERATED_BODY()

	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, Category=Pawn)
	TSoftObjectPtr<UBehaviorTree> BehaviorTree;
};
