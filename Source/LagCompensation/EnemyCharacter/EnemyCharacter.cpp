// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyCharacter.h"

#include "Components/CapsuleComponent.h"

// Sets default values
AEnemyCharacter::AEnemyCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	HeadCapsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("HeadCapsule"));
	HeadCapsule->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, FName("headSocket"));

	BodyCapsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("BodyCapsule"));
	BodyCapsule->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, FName("bodySocket"));

	UpperArmLeft = CreateDefaultSubobject<UCapsuleComponent>(TEXT("UpperArmLeft"));
	UpperArmLeft->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, FName("UpperArmLeft"));

	UpperArmRight = CreateDefaultSubobject<UCapsuleComponent>(TEXT("UpperArmRight"));
	UpperArmRight->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, FName("UpperArmRight"));
	
	LowArmLeft = CreateDefaultSubobject<UCapsuleComponent>(TEXT("LowArmLeft"));
	LowArmLeft->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, FName("LowArmLeft"));

	LowArmRight = CreateDefaultSubobject<UCapsuleComponent>(TEXT("LowArmRight"));
	LowArmRight->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, FName("LowArmRight"));
	
	UpperLegLeft = CreateDefaultSubobject<UCapsuleComponent>(TEXT("UpperLegLeft"));
	UpperLegLeft->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, FName("UpperLegLeft"));

	UpperLegRight = CreateDefaultSubobject<UCapsuleComponent>(TEXT("UpperLegRight"));
	UpperLegRight->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, FName("UpperLegRight"));

	LowLegLeft = CreateDefaultSubobject<UCapsuleComponent>(TEXT("LowLegLeft"));
	LowLegLeft->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, FName("LowLegLeft"));

	LowLegRight = CreateDefaultSubobject<UCapsuleComponent>(TEXT("LowLegRight"));
	LowLegRight->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, FName("LowLegRight"));
	
}

// Called when the game starts or when spawned
void AEnemyCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AEnemyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

